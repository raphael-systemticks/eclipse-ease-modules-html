![Build Status](https://gitlab.com/raphael-systemtick/eclipse-ease-modules-html/badges/master/build.svg)

Update-Site for eclipse-ease-modules can be found here:
https://raphael-systemticks.gitlab.io/eclipse-ease-modules-html/update-site/latest

Only main branch artifacts are uploaded, when triggered manually. 
